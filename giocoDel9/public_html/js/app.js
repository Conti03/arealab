/* 
 * App che simula una calcolatrice
 * 
 */
/* Dizionario Applicazione ad evnti in Javascript */
var APP={
    /*Event CALLBACK/LISTENER + Bind  con Button */
    
/*__________________________________________________________________*/

    /*
     * A-B-C
     * D-E-F
     * G-H-I
     */
    
    
    generateNumeriCasuali : function(){
        var esito = false;
        var numeriCasuali = [];
        while(esito === false){
            var numeroCasuale = Math.floor(Math.random()*(9));
            if (!(numeriCasuali.includes(numeroCasuale))) {
                numeriCasuali.push(numeroCasuale);
            }
            esito = numeriCasuali.length === 9;
        };
        return numeriCasuali;
    },
    
    dizPosizioniBottoni : {A:'#btn1',B:'#btn2',C:'#btn3',D:'#btn4',E:'#btn5',F:'#btn6',G:'#btn7',H:'#btn8',I:'#btn9'},
    dizPosizioniNumeri : { A:0, B:0, C:0, D:0, E:0, F:0, G:0, H:0, I:0},
    dizListaCelleVicine :  { A:['B','D'], B:['A','C','E'], C:['B','F'], D:['A','E','G'], E:['B','D','F','H'], F:['C','E','I'],G:['D','H'], H:['G','E','I'], I:['F','H']},
    numeroMosse : 0,
    
    inizioGioco : function(){
     /*
     * A-B-C
     * D-E-F
     * G-H-I
     */
    
        var numeriCasuali = APP.generateNumeriCasuali();
        var posNC = 0;
        
        for (var key in APP.dizPosizioniNumeri) {
           APP.dizPosizioniNumeri[key]=numeriCasuali[posNC];
           posNC ++;
           };
           
        for (var key in APP.dizPosizioniBottoni) {
            $(APP.dizPosizioniBottoni[key]).html(APP.dizPosizioniNumeri[key]);      
            if(APP.dizPosizioniNumeri[key]==0){
              $(APP.dizPosizioniBottoni[key]).hide();
            }else{
              $(APP.dizPosizioniBottoni[key]).show();  
            };
           
        };
    },
    
    mossa : function(event){
        var btnId = "#"+event.target.id;
        var buttonVuoto ='';
        var buttonPos ='';
        var valbp = '';
        var valbv = '';
        //var buttonVuotoColor='';
        //var buttonPosColor='';
        
        //prendo key che ha id del pulsante pari a quello del pulsante che ha schiacciato
        //(A o B o C ....)
        for(var key in APP.dizPosizioniBottoni){
            if (btnId == APP.dizPosizioniBottoni[key]){
                buttonPos = key;
            };
        };
        
        //prendo keys dei pulsanti di fianco al pulsante schiacciato
        var listTemp =[];
        for (var key of APP.dizListaCelleVicine[buttonPos]) {
            listTemp.push(key);
        }; 
        
        //guardo quale tra i pulsanti vicini a quello schiacciato
        //ha come valore 0 e mi segno la key
        for(var key of listTemp){
            if (APP.dizPosizioniNumeri[key] == 0){
                buttonVuoto = key;
            };
        };
       
     
       
        //mi metto da parte i valori e colori dei 2 pulsanti
        valbv = APP.dizPosizioniNumeri[buttonVuoto];
        valbp = APP.dizPosizioniNumeri[buttonPos];
        //buttonPosColor= $(APP.dizPosizioniBottoni[buttonPos]).css("background-color");
        //buttonVuotoColor= $(APP.dizPosizioniBottoni[buttonVuoto]).css("background-color");
        
       
        
        
        
        //scambio dei colori e valori (sia diz, sia grafico) dei pulsanti ,
        //avverrà solo se le 2 key dei pulsanti non sono ''
        //ovvero funzionerà solo quando uno dei 2 pulsanti ha valore 0
        //(questo in senso pratico)
        if((buttonVuoto != '') && (buttonPos != '')){
            APP.dizPosizioniNumeri[buttonVuoto] = valbp;
            $(APP.dizPosizioniBottoni[buttonVuoto]).html(valbp);
            $(APP.dizPosizioniBottoni[buttonVuoto]).show();
            //$(APP.dizPosizioniBottoni[buttonVuoto]).css('background-color',buttonPosColor);
            
            APP.dizPosizioniNumeri[buttonPos] = valbv;
            $(APP.dizPosizioniBottoni[buttonPos]).html(valbv);
            $(APP.dizPosizioniBottoni[buttonPos]).hide();
            //$(APP.dizPosizioniBottoni[buttonPos]).css('background-color',buttonVuotoColor);
            APP.numeroMosse += 1;
        
            $("#mosse").html("Mosse: "+APP.numeroMosse);
        }
        
        APP.fineGioco();
        
    },
    fineGioco : function (){
        var esito  = '';
        var i=1;
        for (var key in APP.dizPosizioniNumeri) {
            
            if(APP.dizPosizioniNumeri[key]==i){
               esito += 'T'; 
            }else{
               esito += 'F'; 
            };
            i++;
        };
        if (esito == 'TTTTTTTTF') {
            $('#out').html('Gioco finito! Congratulazioni!');
            for (var key in APP.dizPosizioniBottoni) {
                $(APP.dizPosizioniBottoni[key]).attr('disabled', true);
            };
            $("#mosse").html("Ce l'hai fatta in sole " + APP.numeroMosse+" mosse!" );
        };
        
    },
    
    resetGioco : function (){
        $('#out').html('');
        for (var key in APP.dizPosizioniBottoni) {
                $(APP.dizPosizioniBottoni[key]).attr('disabled', false);
            }
        APP.numeroMosse=0;
        $('#mosse').html('Mosse: '+APP.numeroMosse);
        APP.inizioGioco();
    },
/*__________________________________________________________________*/
   
    // Bind di callBack_Sqrt con click su button #Sqrt
    
    
    init_callBack_reset_click : function(){
        $("#reset").on('click', APP.resetGioco);
        
    },
    
    init_callBack_mossa_Click : function(){
        $("#btn1").on('click', APP.mossa);
   
        $("#btn2").on('click', APP.mossa);
    
        $("#btn3").on('click', APP.mossa);
    
        $("#btn4").on('click', APP.mossa);
    
        $("#btn5").on('click', APP.mossa);
    
        $("#btn6").on('click', APP.mossa);
   
        $("#btn7").on('click', APP.mossa);
   
        $("#btn8").on('click', APP.mossa);
    
        $("#btn9").on('click', APP.mossa);
    },
    
    
    
    
/*___________________________________________________________________*/
    
    /* Richiama tutte le funzioni di Bind delle callback */
    init_AllCallback :  function (){
        APP.init_callBack_reset_click();
        APP.init_callBack_mossa_Click();
    }
    
};
/*____________________main()__________________________________________*/
$(document).ready( function(){
    // esegue il bind di tutte le callBack
    APP.init_AllCallback();
    APP.inizioGioco();
    
    
    // main si ferma in attesa di evento
});
