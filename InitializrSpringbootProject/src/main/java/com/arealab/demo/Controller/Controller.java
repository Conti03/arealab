/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.arealab.demo.Controller;

import com.arealab.demo.Controller.Dao.Dao;
import java.util.Enumeration;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Leo
 */
@RestController
public class Controller {
    Dao dao = new Dao();
    
    @RequestMapping(value = "/salvaPartita.htm", method = RequestMethod.GET,
            produces = "application/json")
    @ResponseBody
    public String msg00(HttpServletRequest request) {
        
        
        
        
        String nome = request.getParameter("nome");
        String mosse = request.getParameter("mosse");
        
        //int nmosse = Integer.getInteger(parametri.get("nmosse").toString());
        dao.salvaPartita(nome, mosse );
        
        HashMap<String, String> classifica = dao.getClassifica();
        String out= "{";
        int i =0;
        
        for (String key : classifica.keySet()){
            
            if(i>0){
                out+=",";
            };
            out+="\""+key+"\":\"" + classifica.get(key) + "\"";
            
            i++;
           
        };
        out+="}";
        return (out);
    }
    
    
    @RequestMapping(value = "/getClassifica.htm", method = RequestMethod.GET,
            produces = "application/json;charset=UTF-8")
    @ResponseBody
    public String msg01(HttpServletRequest request) {
        
        HashMap classifica= dao.getClassifica();
        String out= "{";
        for (var key : classifica.keySet()){
            out+="\""+key+"\":\"" + classifica.get(key) + "\",";
        };
        out+="}";
        return (out);
    }
}
