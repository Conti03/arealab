/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.bici.demo.controller;

import com.bici.demo.dao.Dao;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author danie
 */
@RestController
public class Controller {
    Dao dao = new Dao() ;
    
    
    @RequestMapping(value = "/test.htm", method = RequestMethod.GET,
            produces = "application/json")
    @ResponseBody
    public String test(){
        return dao.testDao();
    }
    
    @RequestMapping(value = "/getAllStation.htm", method = RequestMethod.GET,
            produces = "application/json")
    @ResponseBody
    public String getAllStation(){
        HashMap<String, String> stazioni= new HashMap<>();
        stazioni.put("1", "Via le dita dal naso, 32");
        stazioni.put("2", "Via di estinzione, 14");
        stazioni.put("3", "Via Paolo, 24");
        
        String out= "{";
        int i =0;
        
        for (String key : stazioni.keySet()){
            
            if(i>0){
                out+=",";
            };
            out+="\""+key+"\":\"" + stazioni.get(key) + "\"";
            
            i++;
           
        };
        out+="}";
        return out;
    
    }
    
    @RequestMapping(value = "/registrazione.htm", method = RequestMethod.GET,
            produces = "application/json")
    @ResponseBody
    public void registrazioneUtente(){
        
            HashMap<String, ArrayList<String>> utenti= new HashMap<>();
            String idUtente="1";
            String nome="Marco";
            String cognome="Conti";
            String nCartaCredito="12324";
            ArrayList<String> parametriUtente= new ArrayList<>();
            parametriUtente.add(nome);
            parametriUtente.add(cognome);
            parametriUtente.add(nCartaCredito);
            utenti.put(idUtente, parametriUtente);
            System.out.println(utenti.get(idUtente).get(0));
    }
    
}
